'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
 
    var serviceBase = 'http://104.236.116.175:8090/oauth/';
    var authServiceFactory = {};
 
    var _authentication = {
        isAuth: false,
        userName : ""
    };
 
    // var _saveRegistration = function (registration) {
 
    //     _logOut();
 
    //     return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
    //         return response;
    //     });
 
    // };
 
    var _login = function (loginData) {
 
        // For URL encoded 
        // var data = "grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp&username=" + loginData.userName + "&password=" + loginData.password ;

        var deferred = $q.defer();
        
        var headers2 = {
            'Authorization': 'Basic Y2xpZW50YXBwOjEyMzQ1Ng==',
            // 'Content-Type': 'application/x-www-form-urlencoded'
        };
        
        // $http.defaults.headers.common['Authorization'] = 'Basic Y2xpZW50YXBwOjEyMzQ1Ng==';

        // For JSON authorization 
        var data = {
            'grant_type': 'password',
            'scope' : 'read write',
            'username' : loginData.username,
            'password' : loginData.password,
            // withCredentials : true,
            'client_id' : 'clientapp',
            'client_secret' : '123456'
        }
 
        $http.post(serviceBase + 'token', data, { headers: headers2})

        .success(function (response) 
        {
 
            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });
 
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
 
            deferred.resolve(response);
 
        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });
 
        return deferred.promise;
 
    };
 
    var _logOut = function () {
 
        localStorageService.remove('authorizationData');
 
        _authentication.isAuth = false;
        _authentication.userName = "";
 
    };
 
    var _fillAuthData = function () {
 
        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
 
    }
 
    // authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
 
    return authServiceFactory;
}]);